README
======

:Author: Arne Simon [arne.simon@slice-dice.de]

A solution for continous deployment with docker and git.

You can configure your docker containers like with fig or docker-compose, but in
addition to you can specify other projects which will be synced and mounted into
the docker containers.

This is for enviroments in which multiple frequently changing packages, have to be
regulary deployt, wihtout the overhead of creating a new docker image each time.

*Attention:*::

    At this point only git repositories are supported.

Repo
----

    https://bitbucket.org/slicedice/sputnik


Examples
--------

0. (Optional) Maybe on Mac OS you need to run::

    $ $(boot2docker shellinit)

1. Update project references::

    $ sputnik update

2. Start docker container structure::

    $ sputnik up


sputnik.yml
-----------

.. sourcecode:: yaml

    # The referenced projects
    projects:
        # <name-of-folder-in-/tmp> : the git repository.
        made: git@bitbucket.org:slicedice/made.git          # the content of this repo will be mounted in /tmp/made
        pap: git@bitbucket.org:slicedice/valuenet.pap.git   # the content of this repo will be mounted in /tmp/pap
    setup:
        nginx:
            image: slicedice/base
            ports:
                # container: host
                80: 8080    # maps the container port 80 to host 8080
            command: nginx
        mongodb:
            image: 'slicedice/base'
            volumes:
                #  host   container
                - [data/, /data]
            expose:
                - 27017
            command: /usr/bin/mongod --dbpath /data
        stuff:
            image: slicedice/base
            links:
                - mongodb
            working_dir: /tmp/pap
            command: python -m pap.server


Mac OS + MongoDB + Docker Bug
-----------------------------

**Remember!** The setup of mounting the data folder of MongoDB into the host filesystem can be an issue on mac!
Because MongoDB needs the *fsync* call, which is not currently supported through VirtualBox on mac.
