#-*- coding: utf-8 -*-
"""
:Author: Arne Simon [arne.simon@slice-dice.de]
:Copyright: (c) 2015 by Slice + Dice GmbH
"""
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


with open('README.rst') as src:
    long_description = src.read()


with open('requirements.txt') as src:
    requirments = src.read().split('\n')

setup(
    name='sputnik',
    packages=['sputnik'],
    scripts=['bin/sputnik'],
    version='0.3.5',
    install_requires=requirments,
    description='A solution for continous deployment.',
    long_description=long_description,
    author='Arne Simon',
    author_email='arne.simon@slice-dice.de',
    license='MIT',
    url='https://bitbucket.org/slicedice/sputnik',
    keywords=['slice', 'dice', 'docker', 'deployment', 'continous'],
    classifiers=[
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet',
        'Topic :: Office/Business',
    ]
)