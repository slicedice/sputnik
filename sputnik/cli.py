#-*- coding: utf-8 -*-
"""
:Author: Arne Simon [arne.simon@slice-dice.de]
:Copyright: (c) 2015 by Slice + Dice GmbH
"""
from . import dck
from . import raw
from . import common
from docker.errors import APIError
from dockerpty.pty import PseudoTerminal
import click
import json
import os
import sys
import yaml
import subprocess
import time

try:
    import urllib3
    urllib3.disable_warnings()
except:
    import logging
    logging.captureWarnings(True)


VERSION = '0.3.5'


def versionnummber(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return

    click.echo('sputnik version: ' + VERSION)
    ctx.exit()


@click.group()
@click.option('-v', '--version', is_flag=True, callback=versionnummber, is_eager=True, help='Prints sputniks version number.')
@click.option('-c', '--config', default='sputnik.yml', help='The sputnik file to use.')
@click.pass_context
def cli(ctx, version, config):
    ctx.obj['config'] = common.get_config(config)


@cli.add_command
@click.command()
@click.pass_context
def update(ctx):
    """
    Update the git reposetories.
    """
    common.update_repos(ctx.obj['config'])


@cli.add_command
@click.command()
@click.pass_context
def ps(ctx):
    """
    List the state of containers.
    """
    setuptype = ctx.obj['config'].get('type')

    if setuptype == 'docker':
        dck.ps()
    else:
        raw.ps()


@cli.add_command
@click.command()
@click.argument('ctr')
@click.pass_context
def logs(ctx, ctr):
    """
    Logs of the container.
    """
    setuptype = ctx.obj['config'].get('type')

    if setuptype == 'docker':
        dck.logs(ctr)
    else:
        raw.logs(ctr)


@cli.add_command
@click.command()
@click.argument('name')
@click.argument('cmds', nargs=-1)
@click.pass_context
def run(ctx, name, cmds):
    """
    Run a command in a docker container.
    """
    wrkdir = os.getcwd()
    repos = os.path.join(wrkdir, '.sputnik', 'repos')
    repo_name = wrkdir[wrkdir.rfind('/') + 1:]
    config = ctx.obj['config']

    passes = list(common.generate_order_for(name, config['setup']['docker']))
    passes = passes[:-1] # exclude run container

    dck.start_containers(passes, config)

    client = dck.docker()

    with dck.Containers() as containers:
        setup = config.get('setup')

        if setup:
            creation, start = dck.configuration(repos, repo_name, name, config)
            creation['name'] += '_run'

            known = containers.get(creation['name'])

            if known:
                try:
                    client.remove_container(container=known['result']['Id'], force=True)
                except APIError:
                    pass

            creation['command'] = dck.build_command(config, ' '.join(cmds))
            creation['tty'] = True
            creation['stdin_open'] = True

            result = client.create_container(**creation)

            containers[creation['name']] = {
                'creation': creation,
                'start': start,
                'result': result
            }

            PseudoTerminal(client, result['Id'], interactive=True).start(**start)

            client.remove_container(container=result['Id'])
        else:
            click.echo('No setup defined.')


@cli.add_command
@click.command()
@click.argument('id', nargs=-1)
def rm(id):
    """
    Clear all docker containers of this project.
    """
    wrkdir = os.getcwd()
    repo_name = wrkdir[wrkdir.rfind('/')+1:]

    client = dck.docker()

    with dck.Containers() as containers:

        for name, ctr in containers.items():
            if not id or name in id:
                click.echo('remove container {}'.format(name))

                try:
                    client.remove_container(container=ctr['result']['Id'], force=True)
                except APIError:
                    pass

        if id:
            for name in id:
                del containers[name]
        else:
            containers.clear()


@cli.add_command
@click.command()
@click.option('-f', '--force', is_flag=True, help='Forces to rebuild containers instead of just reusing them.')
@click.option('-h', '--host', help='The remote ip or host name.')
@click.argument('name', required=False)
@click.pass_context
def up(ctx, force, host, name):
    """
    Sets up enviroment and starts the containers.
    """
    config = ctx.obj['config']

    if not common.exists_repos(config):
        common.update_repos(config)

    if config.get('type') == 'docker':
        if name:
            passes = common.generate_order_for(name, config['setup']['docker'])
        else:
            passes = common.generate_order(config['setup']['docker'])

        dck.start_containers(passes, config, force)
    else:
        raw.start(config, name, force)


@cli.add_command
@click.command()
@click.argument('name', required=False)
@click.option('-r', '--remove', is_flag=True, help='Stops and removes.')
@click.pass_context
def down(ctx, name, remove):
    """
    Stops all docker containers of this project.
    """
    config = ctx.obj['config']

    if config['type'] == 'docker':
        client = dck.docker()

        def halt(ctr):
            if remove:
                client.remove_container(container=ctr['result']['Id'], force=True)
            else:
                click.echo('stop container {}'.format(name))
                client.stop(container=ctr['result']['Id'])

        if name:
            with dck.Containers() as containers:
                halt(containers[name])
        else:
            with dck.Containers() as containers:
                passes = common.generate_order(config['setup']['docker'])
                passes = reversed(list(passes))

                for name in passes:
                    halt(containers[name])
    else:
        raw.stop()


@cli.add_command
@click.command()
@click.argument('id', nargs=-1)
def reup(id):
    """
    Restart already created containers.
    """
    client = dck.docker()

    with dck.Containers() as containers:
        for name, ctr in containers.items():
            if not id or name in id:
                click.echo('restart container {}'.format(name))
                client.restart(container=ctr['result']['Id'])


@cli.add_command
@click.command()
@click.pass_context
def order(ctx):
    """
    prints order
    """
    config = ctx.obj['config']

    passes = common.generate_order(config['setup']['docker'])

    for name in passes:
        click.echo(name)


if __name__ == '__main__':
    cli(obj={})
