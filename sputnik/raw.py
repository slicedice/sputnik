#-*- coding: utf-8 -*-
"""
:Author: Arne Simon [arne.simon@slice-dice.de]
:Copyright: (c) 2015 by Slice + Dice GmbH
"""
from . import common
from pymongo import MongoClient
import click
import psutil
import pymongo
import os
import signal
import subprocess
import yaml


class Tasks:
    """
    A context for the sputnik task file.
    """
    def __init__(self):
        wrkdir = os.getcwd()

        containersfile = '.sputnik/tasks.yml'
        repos = os.path.join(wrkdir, '.sputnik')

        if not os.path.exists(repos):
            os.makedirs(repos)

        if os.path.exists(containersfile):
            with open('.sputnik/tasks.yml') as src:
                self.containers = yaml.load(src)
        else:
            self.containers = {}

    def __enter__(self):
        return self.containers

    def __exit__(self, *args, **kwargs):
        with open('.sputnik/tasks.yml', 'w') as src:
            src.write(yaml.dump(self.containers))


def ps():
    with Tasks() as tasks:
        for name, task in tasks.items():
            try:
                process = psutil.Process(task['pid'])
                click.echo(' {} | {:10} | {}'.format(task['pid'], name, process.state))
            except psutil.NoSuchProcess:
                pass


def logs(ctr):
    client = MongoClient()
    result = client.made.logs.find({'service_type': ctr})
    result = result.sort('created', pymongo.DESCENDING).limit(20)

    for row in reversed(list(result)):
        click.secho('{created} | {service_type} | {name} | {message}'.format(**row), fg=common.LOG_LEVEL_COLOR[row['level']])


def start(config, id, force):
    wrkdir, repos, repo_name = common.get_dirs()
    cfg_tasks = config['setup']['raw']

    with Tasks() as tasks:
        for name, cfg in cfg_tasks.items():
            if not id or name in id:
                task = tasks.get(name)

                if task:
                    try:
                        process = psutil.Process(task['pid'])
                        task['state'] = process.state

                        if process.state == 'running':

                            if force:
                                click.echo('kill {} ... '.format(name), nl=False)
                                os.kill(task['pid'])
                                task['state'] = 'down'
                    except psutil.NoSuchProcess:
                        task['state'] = 'down'


                if not task or task['state'] == 'down':
                    click.echo('start {}'.format(name))

                    os.chdir(os.path.abspath(cfg_tasks[name]['working_dir']))
                    process = subprocess.Popen(cfg_tasks[name]['command'],
                                               shell=True,
                                               start_new_session=True)

                    tasks[name] = {
                        'pid': process.pid,
                        'state': 'up',
                    }



def stop():
    with Tasks() as tasks:
        for name, task in tasks.items():
            click.echo('kill {}'.format(name))

            if task['state'] == 'up':
                try:
                    os.kill(task['pid'], signal.SIGTERM)
                except ProcessLookupError:
                    pass
                task['state'] = 'down'
