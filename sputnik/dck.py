#-*- coding: utf-8 -*-
"""
:Author: Arne Simon [arne.simon@slice-dice.de]
:Copyright: (c) 2015 by Slice + Dice GmbH
"""
from docker import Client
from docker.utils import kwargs_from_env
import click
import os
import subprocess
import yaml


class Containers:
    """
    A context for the sputnik container file.
    """
    def __init__(self):
        wrkdir = os.getcwd()

        containersfile = '.sputnik/containers.yml'
        repos = os.path.join(wrkdir, '.sputnik')

        if not os.path.exists(repos):
            os.makedirs(repos)

        if os.path.exists(containersfile):
            with open('.sputnik/containers.yml') as src:
                self.containers = yaml.load(src)
        else:
            self.containers = {}

    def __enter__(self):
        return self.containers

    def __exit__(self, *args, **kwargs):
        with open('.sputnik/containers.yml', 'w') as src:
            src.write(yaml.dump(self.containers))


def docker():
    kw = kwargs_from_env()

    if 'tls' in kw:
        kw['tls'].assert_hostname = False
        kw['tls'].verify = False

    return Client(**kw)


def build_command(config, final_command=None):
    commands = [prj['command'] for prj in config['projects'].values()]
    commands = ' && '.join(commands)


    if len(commands) > 0:
        if final_command:
            return "bash -c '{} && exec {}'".format(commands, final_command)
        else:
            return "bash -c '{}'".format(commands)
    else:
        return final_command


def configuration(repos, repo_name, name, config):
    """
    Generate creation and startup parameters for a sputnik container from repo and config.
    """
    cfg = config['setup']['docker'][name]
    creation = {}
    start = {}
    creation.update(cfg)
    creation['name'] = '{}_{}'.format(repo_name, name)

    links = cfg.get('links')
    if links:
        links = [['{}_{}'.format(repo_name, n), n] for n in links]
        del creation['links']
        start['links'] = links

    volumes = creation.get('volumes')
    binds = {}
    start['binds'] = binds
    if volumes:
        del creation['volumes']
        # remeber docker wants the absolut path for the host part folders!
        for host, guest in volumes:
            binds[os.path.abspath(host)] = {'bind': guest, 'ro': False}

    # snow we add to our binds the project folders
    for prjname, prj in config['projects'].items():
        prjfolder = os.path.join(repos, prjname)
        binds[prjfolder] = {'bind': '/tmp/' + prjname, 'ro': False}

    ports = cfg.get('ports')
    if ports:
        creation['ports'] = list(ports.keys())
        start['port_bindings'] = ports

    expose = creation.get('expose')
    if expose:
        del creation['expose']

        if ports:
            creation['ports'].extend(expose)
        else:
            creation['ports'] = expose

    command = creation.get('command')
    if command:
        creation['command'] = build_command(config, command)

    return creation, start


def start_containers(passes, config, force=False):
    wrkdir = os.getcwd()
    repos = os.path.join(wrkdir, '.sputnik', 'repos')
    repo_name = wrkdir[wrkdir.rfind('/') + 1:]
    client = docker()

    with Containers() as containers:

        # we get the containers docker know about
        known_containers = {ctr['Id']: ctr for ctr in client.containers(all=True)}

        # iterate over the config file containers
        for name in passes:
            creation, start = configuration(repos, repo_name, name, config)

            # we check if docker already knows our container
            if name in containers:
                known = known_containers.get(containers[name]['result']['Id'])
            else:
                known = None

            do_start = True

            # if docker knows about our container.
            if known:
                if force:
                    click.echo('kill and setup container {} ... '.format(name), nl=False)

                    client.remove_container(container=known['Id'], force=True)
                    #creation['detach'] = True
                    result = client.create_container(**creation)
                else:
                    result = known
                    do_start = 'Up' not in known['Status']
            else:
                click.echo('setup contrainer {} ... '.format(name), nl=False)

                result = client.create_container(**creation)

            # save the common.configuration, because it could have changed
            # in the sputnik.yml
            containers[name] = {
                'creation':creation,
                'start': start,
                'result': result
            }

            if do_start:
                click.echo('start container {}'.format(name))
                client.start(container=result['Id'], **start)


def ps():
    client = docker()
    dockerctrs = {ctr['Id']: ctr for ctr in client.containers(all=True)}

    def port(p):
        if 'PublicPort' in p:
            return '{PublicPort}->{PrivatePort}:{Type}'.format(**p)
        else:
            return '{PrivatePort}:{Type}'.format(**p)

    with Containers() as containers:
        for c in containers.values():
            ctr = dockerctrs.get(c['result']['Id'])
            if ctr:
                ports = ', '.join([port(p) for p in ctr['Ports']])
                line = '{} | {} | {:30} | {} | {}'.format(ctr['Id'][:14], ctr['Image'], c['creation']['name'], ctr['Status'], ports)
                click.echo(line)


def logs(ctr):
    client = docker()

    with Containers() as containers:
        if ctr in containers:
            logs = client.logs(container=containers[ctr]['result']['Id'])
            click.echo(logs)
