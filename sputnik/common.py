#-*- coding: utf-8 -*-
"""
:Author: Arne Simon [arne.simon@slice-dice.de]
:Copyright: (c) 2015 by Slice + Dice GmbH
"""
import logging
import os
import subprocess
import yaml


LOG_LEVEL_COLOR = {
    logging.DEBUG: 'blue',
    logging.INFO: 'black',
    logging.WARN: 'yellow',
    logging.ERROR: 'red',
    logging.FATAL: 'red'
}


def get_config(config):
    if config.startswith('/'):
        configfile = config
    else:
        wrkdir = os.getcwd()
        configfile = os.path.join(wrkdir, config)

    if not os.path.exists(configfile):
        raise Exception('no ' + configfile)

    with open(configfile) as src:
        return yaml.load(src)


def get_dirs():
    wrkdir = os.getcwd()
    repos = os.path.join(wrkdir, '.sputnik', 'repos')
    repo_name = wrkdir[wrkdir.rfind('/')+1:]

    return wrkdir, repos, repo_name


def exists_repos(config):
    wrkdir = os.getcwd()
    repos = os.path.join(wrkdir, '.sputnik', 'repos')

    for name, repo in config['projects'].items():
        dirname = os.path.join(repos, name)

        if not os.path.exists(dirname):
            return False

    return True


def update_repos(config):
    wrkdir = os.getcwd()
    repos = os.path.join(wrkdir, '.sputnik', 'repos')
    # may be we run in sudo, so we get the username of the real user
    #username = pwd.getpwuid( os.getuid() )[ 0 ]

    if not os.path.exists(repos):
        os.makedirs(repos)

    for name, repo in config['projects'].items():
        os.chdir(repos)
        dirname = os.path.join(repos, name)

        if os.path.exists(dirname):
            os.chdir(dirname)

            subprocess.call('git pull origin {}'.format(repo['branch']), shell=True)
        else:
            subprocess.call('git clone {} {}'.format(repo['repo'], name), shell=True)



def path_stepper(tops, matrix):
    used = set()

    def brows(starts):
        for start in starts:
            if start in matrix:
                for x in brows(matrix[start]):
                    yield x

        for start in starts:
            if start not in used:
                used.add(start)
                yield start

    for x in brows(tops):
        yield x


def generate_order(containers):
    tops = set()
    matrix = {}

    def cnv(x):
        if type(x) == list:
            return x[1]
        else:
            return x

    for name, ctr in containers.items():
        links = ctr.get('links')

        if links:
            matrix[name] = {cnv(l) for l in links}
            tops.add(name)

    for name in matrix.keys():
        tops -= matrix[name]

    return path_stepper(tops, matrix)


def generate_order_for(module, containers):
    tops = {module}
    matrix = {}

    def cnv(x):
        if type(x) == list:
            return x[1]
        else:
            return x

    for name, ctr in containers.items():
        links = ctr.get('links')

        if links:
            matrix[name] = {cnv(l) for l in links}

    return path_stepper(tops, matrix)
